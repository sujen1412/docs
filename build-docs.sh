#!/usr/bin/env bash


set -o errexit

GIT_URL="https://gitlab.com/datadrivendiscovery/d3m.git"
BRANCH="devel"
CURR_DIR=$PWD
PUBLIC_PATH="$CURR_DIR/public"
VERSION_FILE="$CURR_DIR/versions.txt"

# Build for devel branch
./build-docs-for-ref.sh $GIT_URL $BRANCH $VERSION_FILE $PUBLIC_PATH
# Get all TAGS
for TAG in $(git ls-remote --tags https://gitlab.com/datadrivendiscovery/d3m.git | sed -E 's/^[[:xdigit:]]+[[:space:]]+refs\/tags\/(.+)/\1/g');
do
    ./build-docs-for-ref.sh $GIT_URL $TAG $VERSION_FILE $PUBLIC_PATH
done
python3 build-site.py $VERSION_FILE
exit 0

