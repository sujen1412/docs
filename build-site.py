import os
import sys
from jinja2 import Template


PUBLIC_DIR = "public"


def main(args):
    versions_file = args[0]
    versions = list()
    with open(versions_file) as fr:
        for line in fr:
            versions.append(line.strip("\n"))
    print("Found {} versions".format(versions))
    if len(versions) < 1:
        print("No versions found, exiting..")
        exit(0)
    with open('templates/index.template') as file_:
        template = Template(file_.read())
    index_html = template.render(versions_list=versions)
    if not os.path.exists(PUBLIC_DIR):
        os.mkdir(PUBLIC_DIR)
    with open(os.path.join(PUBLIC_DIR, "index.html"), 'w') as fw:
        fw.write(index_html)


if __name__ == '__main__':
    main(sys.argv[1:])


